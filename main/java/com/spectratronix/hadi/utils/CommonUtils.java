package com.spectratronix.hadi.utils;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.estimote.coresdk.observation.utils.Proximity;
import com.estimote.coresdk.recognition.packets.Beacon;
import com.spectratronix.hadi.R;
import com.spectratronix.hadi.view.activity.MainActivity;
import com.spectratronix.hadi.viewmodel.receiver.DoNotDisturbAlarmReceiver;

import java.util.Calendar;


/**
 * Created by ashutosh on 29/5/18.
 */


@SuppressWarnings("ALL")
public class CommonUtils {


    /* set color as per sdk level */
    public static int getColor(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }

    public static Drawable getDrawable(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getResources().getDrawable(id, null);
        } else {
            return context.getResources().getDrawable(id);
        }
    }

    public static void setFragment(Fragment fragment, boolean removeStack, FragmentActivity activity, int mContainer) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction ftTransaction = fragmentManager.beginTransaction();
        if (removeStack) {
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            ftTransaction.replace(mContainer, fragment);
        } else {
            ftTransaction.replace(mContainer, fragment);
            ftTransaction.addToBackStack(null);
        }
        ftTransaction.commit();
    }

    public static void setFragmentChild(Fragment fragment, boolean removeStack, int mContainer, FragmentManager fm) {

        FragmentManager fragmentManager = fm;
        FragmentTransaction ftTransaction = fragmentManager.beginTransaction();

        if (removeStack) {
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            ftTransaction.replace(mContainer, fragment);
        } else {
            ftTransaction.replace(mContainer, fragment);
            ftTransaction.addToBackStack(null);
        }

        ftTransaction.commit();

    }


    public static void showNotification(Context context, String title, String message) {

        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivities(context, 0, new Intent[]{intent}, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap notifyImage = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        String CHANNEL_ID = "my_channel_01";
        // Build the notification and add the action.


        Notification notification;
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            //This only needs to be run on Devices on Android O and above
            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            String id = "YOUR_CHANNEL_ID";
            CharSequence name = "YOUR_CHANNEL NAME"; //user visible
            String description = "YOUR_CHANNEL_DESCRIPTION"; //user visible
            int importance = NotificationManager.IMPORTANCE_MAX;
            @SuppressLint("WrongConstant")
            NotificationChannel mChannel = new NotificationChannel(id, name, importance);
            mChannel.setDescription(description);
            mChannel.enableLights(true);
            mChannel.setLightColor(R.color.colorPrimary);
            mChannel.enableVibration(true);
            mChannel.canShowBadge();
            mChannel.setShowBadge(true);
            mChannel.setVibrationPattern(new long[]{0, 1000});
            mNotificationManager.createNotificationChannel(mChannel);
            notification = new Notification.Builder(context, "YOUR_CHANNEL_ID")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(title)
                    .setTicker(title)
                    .setLargeIcon(notifyImage)
                    .setContentText(message)
                    .setAutoCancel(true)
//                        .setLargeIcon(Bitmap.createScaledBitmap(notifyImage, 128, 128, false))
                    .setContentIntent(pendingIntent)
                    .setOngoing(false).build();
            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.notify(0, notification);
            }
        } else {
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(title)
                    .setLargeIcon(notifyImage)
                    .setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(NotificationManager.IMPORTANCE_HIGH)
                    .setColor(context.getResources().getColor(R.color.colorPrimary))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri);
            notificationBuilder.setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.notify(0, notificationBuilder.build());
            }
        }


    }

    public static void startDoNotDisturbAlarm(Context context, int timeMin) {
        Calendar calendar = Calendar.getInstance();
        Intent intent = new Intent(context, DoNotDisturbAlarmReceiver.class);
        PendingIntent pintent = PendingIntent.getBroadcast(context, 2, intent, 0);
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarm.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis()+(timeMin * 60 * 1000), pintent);
    }
}
