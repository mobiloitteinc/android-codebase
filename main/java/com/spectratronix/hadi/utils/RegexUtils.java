package com.spectratronix.hadi.utils;

import android.util.Patterns;
import java.util.regex.Pattern;

/**
 * Created by Ashutosh Kumar on 1/6/2018.
 */

public class RegexUtils {

    public static final String PASSWORD_REGEX = "^(?=.*\\d)(?=.*[A-Z]).{8,16}$";

    private final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public RegexUtils() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    public static boolean isValidEmail(CharSequence target) {
        return target != null && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isValidMobile(String name) {
        String regex = "^([6-9\\+]|\\(\\d{1,3}\\))[0-9\\-\\. ]{7,13}$";
        return Pattern.matches(regex, name);
    }

}
