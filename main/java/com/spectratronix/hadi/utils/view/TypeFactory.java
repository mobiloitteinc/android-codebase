package com.spectratronix.hadi.utils.view;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by ashutosh on 29/5/18.
 */

public class TypeFactory {

    Typeface bold;
    Typeface regular;
    Typeface medium;
    Typeface light;

    public TypeFactory(Context context) {
        String BOLD = "dubai_bold.ttf";
        bold = Typeface.createFromAsset(context.getAssets(), BOLD);

        String REGULAR = "dubai_regular.ttf";
        regular = Typeface.createFromAsset(context.getAssets(), REGULAR);

        String MEDIUM= "dubai_medium.ttf";
        medium = Typeface.createFromAsset(context.getAssets(), MEDIUM);

        String LIGHT = "dubai_light.ttf";
        light = Typeface.createFromAsset(context.getAssets(), LIGHT);
    }
}
