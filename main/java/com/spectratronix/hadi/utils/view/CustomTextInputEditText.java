package com.spectratronix.hadi.utils.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;

import com.spectratronix.hadi.R;

public class CustomTextInputEditText extends TextInputEditText {

    private TypeFactory typeFactory;

    public CustomTextInputEditText(Context context) {
        super(context);
    }

    public CustomTextInputEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context, attrs);
    }

    public CustomTextInputEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context, attrs);
    }

    private void applyCustomFont(Context context, AttributeSet attributeSet) {
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                attributeSet, R.styleable.CustomTextView, 0, 0
        );
        int typefaceType;
        try {
            typefaceType = typedArray.getInteger(R.styleable.CustomTextView_font_name, 0);
        } finally {
            typedArray.recycle();
        }
        if (!isInEditMode()) {
            setTypeface(getTypeFace(typefaceType));
        }
    }

    private Typeface getTypeFace(int type) {
        if (typeFactory == null) {
            typeFactory = new TypeFactory(getContext());
        }

        switch (type) {
            case ViewConstants.BOLD:
                return typeFactory.bold;
            case ViewConstants.REGULAR:
                return typeFactory.regular;
            case ViewConstants.MEDIUM:
                return typeFactory.medium;
            case ViewConstants.LIGHT:
                return typeFactory.light;
            default:
                return typeFactory.regular;
        }
    }
}
