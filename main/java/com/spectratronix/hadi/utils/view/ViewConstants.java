package com.spectratronix.hadi.utils.view;

/**
 * Created by ashutosh on 29/5/18.
 */

public interface ViewConstants {

    int BOLD =1, REGULAR = 2, MEDIUM = 3, LIGHT = 4;

}
