package com.spectratronix.hadi.viewmodel.main;

public interface PageChangeListeners {

    void onChangePage();
}
