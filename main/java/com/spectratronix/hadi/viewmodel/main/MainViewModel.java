package com.spectratronix.hadi.viewmodel.main;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.estimote.coresdk.common.requirements.SystemRequirementsChecker;
import com.spectratronix.hadi.R;
import com.spectratronix.hadi.utils.PrefManager;
import com.spectratronix.hadi.databinding.DialogAlertsBinding;
import com.spectratronix.hadi.utils.AppConstants;
//import com.spectratronix.hadi.utils.SyncUtils;
import com.spectratronix.hadi.utils.KeyboardUtils;
import com.spectratronix.hadi.utils.ToastUtils;
import com.spectratronix.hadi.view.activity.MainActivity;
import com.spectratronix.hadi.view.fragment.HomeFragment;
import com.spectratronix.hadi.view.fragment.SettingsFragment;
import com.spectratronix.hadi.viewmodel.base.activity.ActivityViewModel;
import com.spectratronix.hadi.viewmodel.receiver.BeaconStatusReceiver;
import com.spectratronix.hadi.viewmodel.receiver.LocationStateReceiver;

import java.util.Calendar;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

/**
 * Created by ashutosh on 29/5/18.
 */


public class MainViewModel extends ActivityViewModel<MainActivity> implements PageChangeListeners{


    private MainActivity activity;
    private ViewPagerAdapter pagerAdapter;
    private BluetoothAdapter bluetoothAdapter;
    private final static int REQUEST_ENABLE_BT = 1;
    private PrefManager prefManager;
    private String locale;



    public MainViewModel(MainActivity activity) {
        super(activity);
        this.activity = activity;
        prefManager = PrefManager.getInstance(activity);
        initializedControl();
        checkLocationStatus();
        checkBeaconStatus();
    }



    private void initializedControl() {

//        String dndTime = prefManager.getPreference(AppConstants.DND_TIME, "20");
//        if(StringUtils.isBlank(dndTime)){
//            prefManager.savePreference(AppConstants.DND_TIME_ACTIVATE, false);
//        }

//        boolean isDndFirst = prefManager.getPreference(AppConstants.IS_DND_FIRST, false);
//        if(isDndFirst){
//            prefManager.savePreference(AppConstants.DND_TIME_ACTIVATE, false);
//        }

        locale = Locale.getDefault().getLanguage();


        checkLocationSetting();
        initializedBluetooth();
        if (bluetoothAdapter == null) {
            ToastUtils.showToastShort(activity, activity.getString(R.string.your_device_does_not_support_bluetooth));
        } else {
            if (!bluetoothAdapter.isEnabled()) {
                showAlert();
            }
        }



        pagerAdapter = new ViewPagerAdapter(activity.getSupportFragmentManager());
        activity.getBinding().viewPager.setAdapter(pagerAdapter);
        activity.getBinding().viewPager.setCurrentItem(1);

        activity.getBinding().toolBar.ivLeft.setOnClickListener(v -> {
            if(activity.getBinding().viewPager.getCurrentItem()==1){
                activity.getBinding().viewPager.setCurrentItem(0);
            } else if(activity.getBinding().viewPager.getCurrentItem()==2){
                activity.getBinding().viewPager.setCurrentItem(1);
            } else if(activity.getBinding().viewPager.getCurrentItem()==3){
                activity.getBinding().viewPager.setCurrentItem(2);
            } else{
                activity.getBinding().viewPager.setCurrentItem(1);
            }

        });

        activity.getBinding().toolBar.ivRight.setOnClickListener(v -> {
            if(activity.getBinding().viewPager.getCurrentItem()==1){
                activity.getBinding().viewPager.setCurrentItem(2);
            } else if(activity.getBinding().viewPager.getCurrentItem()==2){
                activity.getBinding().viewPager.setCurrentItem(3);
            } else{
                activity.getBinding().viewPager.setCurrentItem(1);
            }


        });

        activity.getBinding().viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                switch (position) {
                    case 0:
                        if(locale.equalsIgnoreCase("ar")) {
                            activity.getBinding().toolBar.ivRight.setRotation(0);
                        } else {
                            activity.getBinding().toolBar.ivRight.setRotation(180);
                        }
                        activity.getBinding().toolBar.llLeft.setVisibility(View.GONE);
                        activity.getBinding().toolBar.llRight.setVisibility(View.VISIBLE);
                        activity.getBinding().toolBar.ivRight.setImageResource(R.mipmap.back_button);
//                        activity.getBinding().toolBar.ivRight.setRotation(180);
                        activity.getBinding().toolBar.tvHeader.setText(R.string.settings);
                        break;
                    case 1:
                        activity.getBinding().toolBar.llLeft.setVisibility(View.VISIBLE);
                        activity.getBinding().toolBar.llRight.setVisibility(View.VISIBLE);
                        activity.getBinding().toolBar.ivLeft.setImageResource(R.mipmap.settings_icon);
                        activity.getBinding().toolBar.ivRight.setImageResource(R.mipmap.mobile_silence_white);
                        activity.getBinding().toolBar.tvHeader.setText(R.string.home);
                        activity.getBinding().toolBar.ivRight.setRotation(0);
                        KeyboardUtils.hideKeyboard(activity);
                        break;
                    case 2:
                        if(locale.equalsIgnoreCase("ar")) {
                            activity.getBinding().toolBar.ivRight.setRotation(0);
                            activity.getBinding().toolBar.ivLeft.setRotation(180);
                        } else {
                            activity.getBinding().toolBar.ivLeft.setRotation(0);
                            activity.getBinding().toolBar.ivRight.setRotation(180);
                        }
                        activity.getBinding().toolBar.llLeft.setVisibility(View.VISIBLE);
                        activity.getBinding().toolBar.llRight.setVisibility(View.VISIBLE);
                        activity.getBinding().toolBar.ivLeft.setImageResource(R.mipmap.back_button);
                        activity.getBinding().toolBar.ivRight.setImageResource(R.mipmap.back_button);
//                        activity.getBinding().toolBar.ivRight.setRotation(180);
                        activity.getBinding().toolBar.tvHeader.setText(R.string.about_us);
                        break;
                     case 3:
                        activity.getBinding().toolBar.llLeft.setVisibility(View.VISIBLE);
                        activity.getBinding().toolBar.llRight.setVisibility(View.GONE);
                        activity.getBinding().toolBar.ivLeft.setImageResource(R.mipmap.back_button);
                        activity.getBinding().toolBar.tvHeader.setText(R.string.request_hadi_);
                        break;

                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    private void initializedBluetooth() {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    @Override
    public void onResume() {
        super.onResume();
        SystemRequirementsChecker.checkWithDefaultDialogs(activity);
        prefManager.savePreference(AppConstants.IS_FORGROUND, true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        prefManager.savePreference(AppConstants.IS_FORGROUND, false);
    }

    @Override
    public void onChangePage() {
        activity.getBinding().viewPager.setCurrentItem(3);
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new SettingsFragment();
                case 1:
                    return new HomeFragment();
                case 2:
                    return new AboutUsFragment();
                case 3:
                    return new BeaconsFragment();

                default:
                    return null;

            }
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (BeaconsFragment.beaconsViewModel != null) {
            BeaconsFragment.beaconsViewModel.onActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {

            } else {

            }
        }
    }


    private void showAlert() {
        final Dialog mDialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(false);
        if (mDialog.getWindow() != null) {
            mDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            mDialog.getWindow().setGravity(Gravity.CENTER);
            WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
            lp.dimAmount = 0.75f;
            mDialog.getWindow()
                    .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mDialog.getWindow();
            mDialog.getWindow().setAttributes(lp);
            DialogAlertsBinding dialogBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_alerts, null, false);
            mDialog.setContentView(dialogBinding.getRoot());
            dialogBinding.tvOk.setOnClickListener(v -> {
                mDialog.dismiss();
                Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                activity.startActivityForResult(intent, REQUEST_ENABLE_BT);
            });
            mDialog.show();
        }
    }

    protected void checkLocationSetting() {
        if (!checkPermission()) {
            requestPermission();
        }
    }

    /**
     * check permission for location
     */
    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * request permission for location
     */
    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1001);
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1001);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1001:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ToastUtils.showToastShort(activity, activity.getString(R.string.permission_granted));
                } else {
                    ToastUtils.showToastShort(activity, activity.getString(R.string.permission_denied));
                    finish();
                }
                break;
        }
    }

    private void checkLocationStatus() {
        Calendar calendar = Calendar.getInstance();
        Intent intent = new Intent(activity, LocationStateReceiver.class);
        PendingIntent pintent = PendingIntent.getBroadcast(activity, 2, intent, 0);
        AlarmManager alarm = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 60*1000, pintent);
    }

    private void checkBeaconStatus() {
        Calendar calendar = Calendar.getInstance();
        Intent intent = new Intent(activity, BeaconStatusReceiver.class);
        PendingIntent pintent = PendingIntent.getBroadcast(activity, 2, intent, 0);
        AlarmManager alarm = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), /*60**/60*1000, pintent);
    }


}
