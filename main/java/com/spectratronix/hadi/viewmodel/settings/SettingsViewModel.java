package com.spectratronix.hadi.viewmodel.settings;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.spectratronix.hadi.utils.CommonUtils;
import com.spectratronix.hadi.utils.PrefManager;
import com.spectratronix.hadi.databinding.FragmentSettingsBinding;
import com.spectratronix.hadi.utils.AppConstants;
import com.spectratronix.hadi.viewmodel.receiver.DoNotDisturbAlarmReceiver;
import com.spectratronix.hadi.utils.StringUtils;
import com.spectratronix.hadi.view.fragment.SettingsFragment;
import com.spectratronix.hadi.viewmodel.base.fragment.FragmentViewModel;

import java.util.Calendar;

/**
 * Created by ashutosh on 29/5/18.
 */


public class SettingsViewModel extends FragmentViewModel<SettingsFragment, FragmentSettingsBinding>{

    private PrefManager prefManager;

    public SettingsViewModel(SettingsFragment fragment, FragmentSettingsBinding binding) {
        super(fragment, binding);
    }

    @Override
    protected void initialize(FragmentSettingsBinding binding) {
        prefManager = PrefManager.getInstance(getActivity());
        String dndTime = prefManager.getPreference(AppConstants.DND_TIME, "20");
        if(!StringUtils.isBlank(dndTime)){
            binding.ietTime.setText(dndTime);
        } /*else {
            prefManager.savePreference(AppConstants.DND_TIME_ACTIVATE, true);
        }*/
        boolean isAutoRinger = prefManager.getPreference(AppConstants.AUTO_RINGER_SILENT, false);
        if(isAutoRinger){
            binding.switchAuto.setChecked(true);
        } else {
            binding.switchAuto.setChecked(false);
        }

        binding.ietTime.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if(StringUtils.isBlank(binding.ietTime.getText().toString().trim())){
//                        prefManager.savePreference(AppConstants.DND_TIME_ACTIVATE, false);
                    } else {
                        prefManager.savePreference(AppConstants.DND_TIME, binding.ietTime.getText().toString().trim());
//                        prefManager.savePreference(AppConstants.DND_TIME_ACTIVATE, true);
                        int time = Integer.parseInt(binding.ietTime.getText().toString().trim());
                        CommonUtils.startDoNotDisturbAlarm(getActivity(), time);
//                        setDND(time);
                    }
                }
                return false;
            }
        });

    }

    public TextWatcher getDndTime(){
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String text = s.toString();
                /*if(StringUtils.isBlank(text)){
                    prefManager.savePreference(AppConstants.DND_TIME_ACTIVATE, false);
                } else {
                    prefManager.savePreference(AppConstants.DND_TIME, text.trim());
                    prefManager.savePreference(AppConstants.DND_TIME_ACTIVATE, true);
                    int time = Integer.parseInt(text);
                    setDND(time);
                }*/

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

   /* private void setDND(int timeMin){
        Calendar calendar = Calendar.getInstance();
        Intent intent = new Intent(getActivity(), DoNotDisturbAlarmReceiver.class);
        PendingIntent pintent = PendingIntent.getBroadcast(getActivity(), 2, intent, 0);
        AlarmManager alarm = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        alarm.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis()+(timeMin * 60 * 1000), pintent);

    }
*/
    public void automaticallySilent(){
        if(getBinding().switchAuto.isChecked()) {
            prefManager.savePreference(AppConstants.AUTO_RINGER_SILENT, true);
        } else {
            prefManager.savePreference(AppConstants.AUTO_RINGER_SILENT, false);
        }
    }
}
