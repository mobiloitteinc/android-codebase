package com.spectratronix.hadi.viewmodel.home;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.spectratronix.hadi.R;
import com.spectratronix.hadi.databinding.FragmentHomeBinding;
import com.spectratronix.hadi.view.fragment.HomeFragment;
import com.spectratronix.hadi.viewmodel.base.fragment.FragmentViewModel;

/**
 * Created by ashutosh on 29/5/18.
 */

public class HomeViewModel extends FragmentViewModel<HomeFragment, FragmentHomeBinding>{


    public HomeViewModel(HomeFragment fragment, FragmentHomeBinding binding) {
        super(fragment, binding);
    }

    @Override
    protected void initialize(FragmentHomeBinding binding) {

    }


}
