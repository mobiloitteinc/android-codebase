package com.spectratronix.hadi.view.activity;

import android.os.Handler;
import android.widget.Toast;

import com.spectratronix.hadi.R;
import com.spectratronix.hadi.BR;
import com.spectratronix.hadi.databinding.ActivityMainBinding;
import com.spectratronix.hadi.viewmodel.base.activity.BindingActivity;
import com.spectratronix.hadi.viewmodel.main.MainViewModel;
import com.spectratronix.hadi.viewmodel.main.PageChangeListeners;

/**
 * Created by ashutosh on 29/5/18.
 */

public class MainActivity extends BindingActivity<ActivityMainBinding, MainViewModel> implements PageChangeListeners {

    private boolean doubleBackToExitPressedOnce = false;

    @Override
    public MainViewModel onCreate() {
        return new MainViewModel(this);
    }

    @Override
    public int getVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void onChangePage() {
        new MainViewModel(this).onChangePage();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce=false, 2000);
    }
}
