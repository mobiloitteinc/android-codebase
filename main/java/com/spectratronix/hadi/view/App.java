package com.spectratronix.hadi.view;

import android.content.Context;
import android.media.AudioManager;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import com.crashlytics.android.Crashlytics;
import com.estimote.coresdk.common.config.EstimoteSDK;
import com.estimote.coresdk.service.BeaconManager;
import com.spectratronix.hadi.utils.PrefManager;
import com.spectratronix.hadi.viewmodel.beacons.MyBeaconManager;
import io.fabric.sdk.android.Fabric;

public class App extends MultiDexApplication {

    private BeaconManager beaconManager;
    private final String TAG = App.class.getSimpleName();
    private PrefManager prefManager;
    private AudioManager audioManager;


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        prefManager = PrefManager.getInstance(getApplicationContext());
        beaconManager = new BeaconManager(getApplicationContext());
        audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        EstimoteSDK.enableDebugLogging(true);

        MyBeaconManager myBeaconManager = new MyBeaconManager(getApplicationContext(), beaconManager);
        myBeaconManager.initialisedBeacons();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


}
