package com.spectratronix.hadi.view.fragment;

import com.spectratronix.hadi.R;
import com.spectratronix.hadi.BR;
import com.spectratronix.hadi.databinding.FragmentHomeBinding;
import com.spectratronix.hadi.viewmodel.base.fragment.BindingFragment;
import com.spectratronix.hadi.viewmodel.home.HomeViewModel;

public class HomeFragment extends BindingFragment<HomeViewModel, FragmentHomeBinding> {
    @Override
    protected HomeViewModel onCreateViewModel(FragmentHomeBinding binding) {
        return new HomeViewModel(this, getBinding());
    }

    @Override
    public int getVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutResources() {
        return R.layout.fragment_home;
    }
}
