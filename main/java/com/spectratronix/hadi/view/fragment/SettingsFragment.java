package com.spectratronix.hadi.view.fragment;

import android.support.v4.app.Fragment;

import com.spectratronix.hadi.R;
import com.spectratronix.hadi.BR;
import com.spectratronix.hadi.databinding.FragmentSettingsBinding;
import com.spectratronix.hadi.viewmodel.base.fragment.BindingFragment;
import com.spectratronix.hadi.viewmodel.settings.SettingsViewModel;

public class SettingsFragment extends BindingFragment<SettingsViewModel, FragmentSettingsBinding> {

    @Override
    protected SettingsViewModel onCreateViewModel(FragmentSettingsBinding binding) {
        return new SettingsViewModel(this, getBinding());
    }

    @Override
    public int getVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutResources() {
        return R.layout.fragment_settings;
    }

}
